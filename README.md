# README #

Here is an example CUDA implementation of a spiking neural network model for midbrain superior colliculus. stateUpdate.cu simulates neural network activity for different eye movement targets and save them under run/ folder. Simulation results are then analysed by the ipython notebook Analysis.ipynb. Data from an example run is already uploaded as well with plots under results/ folder.